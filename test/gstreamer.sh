#!/bin/bash -f

if [ $# != 1 ]; then
	echo "Usage: "$(basename $0)" video.MOV"
	exit -1
fi

videoIn=$1
video=${videoIn%.*}

width=400
height=224
framerate=10		 				# 10 instead of 30fps, to slow down the video and see artifacts
framerate_i=$(( framerate / 2 ))	# interlaced video are half rate
videoparse="videoparse format=GST_VIDEO_FORMAT_RGBA width=$width height=$height"
videoparse_p="$videoparse framerate=$framerate/1"
videoparse_i="$videoparse framerate=$framerate_i/1 interlaced=true top-field-first=1"


toDisplay="videoconvert ! autovideosink"

function toRawFile()
{
	file=$1
	echo -n "videoconvert ! video/x-raw, format=RGBA ! filesink location=${file}.raw"
	return 0
}

function toMp4()
{
	file=$1
	echo -n "queue ! videoconvert ! x264enc quantizer=30 ! video/x-h264, profile=high ! mp4mux ! filesink location=${file}.mp4"
	return 0
}

# Convert to raw RGBA $widthx$height
echo ; echo "Original Video:"
gst-launch-1.0 filesrc location=$videoIn ! qtdemux name=demux demux.video_0 ! queue ! decodebin ! videoscale ! video/x-raw, width=$width, height=$height ! videoflip method=rotate-180 ! $(toRawFile ${video})
# Play back
gst-launch-1.0 filesrc location=${video}.raw ! $videoparse_p ! $toDisplay

# Interlace frames @15fps
echo ; echo "Interlaced Video:"
gst-launch-1.0 filesrc location=${video}.raw ! $videoparse_p ! videoconvert ! interlace field-pattern=1:1 top-field-first=1 ! $(toRawFile ${video}_i)
# Play back
gst-launch-1.0 filesrc location=${video}_i.raw ! $videoparse_i ! $toDisplay

# Deinterlace frames @30fps
echo ; echo "Deinterlace with proprietary Algo:"
cat ${video}_i.raw | ../demo_deinterlace ${width}x${height} > ${video}_di.raw
# Play back
gst-launch-1.0 filesrc location=${video}_di.raw ! $videoparse_p ! $toDisplay

# Store in Mkv
gst-launch-1.0 filesrc location=${video}.raw    ! $videoparse_p ! $(toMp4 ${video})
gst-launch-1.0 filesrc location=${video}_i.raw  ! $videoparse_i ! $(toMp4 ${video}_i)
gst-launch-1.0 filesrc location=${video}_di.raw ! $videoparse_p ! $(toMp4 ${video}_di)


# Deinterlace using Gstreamer algo

function gstDeinterlace()
{
	method=$1
	name=$2
	echo ; echo "Deinterlace with gst method $name:"
	# Play Back
	gst-launch-1.0 filesrc location=${video}_i.raw ! $videoparse_i ! deinterlace fields=2 method=$method ! $toDisplay
	# Store in Mkv
	gst-launch-1.0 filesrc location=${video}_i.raw ! $videoparse_i ! deinterlace fields=2 method=$method ! $(toMp4 ${video}_$name)
}

gstDeinterlace 0 tomsmocomp
gstDeinterlace 1 greedy_h
gstDeinterlace 2 greedy_l
gstDeinterlace 3 vfyr
gstDeinterlace 4 linear
gstDeinterlace 5 linear_blend
gstDeinterlace 6 scale_bob
gstDeinterlace 7 weave
gstDeinterlace 8 weave_tff
gstDeinterlace 9 weave_bff

# Deinterlace using Yadif algo
echo ; echo "Deinterlace with gst method Yadif:"
gst-launch-1.0 filesrc location=${video}_i.raw ! $videoparse_i ! videoconvert ! yadif ! $toDisplay
gst-launch-1.0 filesrc location=${video}_i.raw ! $videoparse_i ! videoconvert ! yadif ! $(toMp4 ${video}_yadif)

