# For Linux compilation: export CC=g++
# For Arm64 Linux cross compilation: export CC=aarch64-linux-gnu-g++
# For Android cross compilation: export CC=${NDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android22-clang

CCFLAGS := -Iinclude -flax-vector-conversions
OUTPUT_PATH ?= .

all: ${OUTPUT_PATH}/libdeinterlace.so ${OUTPUT_PATH}/libdeinterlace.a ${OUTPUT_PATH}/test_deinterlace ${OUTPUT_PATH}/demo_deinterlace

clean:
	rm -f ${OUTPUT_PATH}/libdeinterlace.so ${OUTPUT_PATH}/libdeinterlace.a deinterlace.o deinterlaceNeon16.o

${OUTPUT_PATH}/libdeinterlace.so: deinterlace.o deinterlaceNeon16.o
	mkdir -p ${OUTPUT_PATH}
	$(CC) -shared -o $@ deinterlace.o deinterlaceNeon16.o -fPIC $(CCFLAGS)

deinterlace.o: src/deinterlace.cpp  include/deinterlace.h
	$(CC) -o $@ -c src/deinterlace.cpp $(CCFLAGS)

deinterlaceNeon16.o: src/deinterlaceNeon16.cpp  include/deinterlace.h
	$(CC) -o $@ -c src/deinterlaceNeon16.cpp $(CCFLAGS)

${OUTPUT_PATH}/libdeinterlace.a: deinterlace.o deinterlaceNeon16.o
	mkdir -p ${OUTPUT_PATH}
	ar rcs $@ deinterlace.o deinterlaceNeon16.o

${OUTPUT_PATH}/test_deinterlace: test/test_deinterlace.cpp ${OUTPUT_PATH}/libdeinterlace.a
	mkdir -p ${OUTPUT_PATH}
	$(CC) -static -o $@ test/test_deinterlace.cpp ${OUTPUT_PATH}/libdeinterlace.a $(CCFLAGS)

${OUTPUT_PATH}/demo_deinterlace: test/demo_deinterlace.cpp ${OUTPUT_PATH}/libdeinterlace.a
	mkdir -p ${OUTPUT_PATH}
	$(CC) -o $@ test/demo_deinterlace.cpp ${OUTPUT_PATH}/libdeinterlace.a $(CCFLAGS)

