/*
 * Copyright (c) 2020 Renault S.A.S.
 * This Source Code Form is subject to the terms of the Mozilla Public * License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * you can obtain one at https://mozilla.org/MPL/2.0/.
 */

#if defined (__ARM_NEON)
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <arm_neon.h>

#include "deinterlace.h"

// Enable this to highlight impacted pixels
//#define COLORED

// Enable this to identify functions impacting performances (see simpleperf tool)
//#define INLINE __attribute__( ( noinline ) )
#define INLINE  //__attribute__( ( always_inline ) )

// Threshold for movement detection [0-255]
// Boosted by 20% to adjust with the approximative luminosity algo (see pixelLumNeon).
#define DELTA_LUM_THRESHOLD ((int)(20 * 0.8))

uint8x16x4_t pixelColorNeon16(int color) INLINE;
uint8x16x4_t pixelColorNeon16(int color) {
    uint8x16x4_t pixel;

    switch (color) {
        case 0:
            pixel.val[0] = vdupq_n_u8(0xff);
            pixel.val[1] = vdupq_n_u8(0x00);
            pixel.val[2] = vdupq_n_u8(0x00);
            break;
        case 1:
            pixel.val[0] = vdupq_n_u8(0x00);
            pixel.val[1] = vdupq_n_u8(0xff);
            pixel.val[2] = vdupq_n_u8(0x00);
            break;
        case 2:
            pixel.val[0] = vdupq_n_u8(0x00);
            pixel.val[1] = vdupq_n_u8(0x00);
            pixel.val[2] = vdupq_n_u8(0xff);
            break;
        case 3:
            pixel.val[0] = vdupq_n_u8(0x00);
            pixel.val[1] = vdupq_n_u8(0x00);
            pixel.val[2] = vdupq_n_u8(0x00);
            break;
        default:
            break;
    }
    pixel.val[3] = vdupq_n_u8(0xff);

    return pixel;
}

uint8x16_t pixelLumNeon16(uint8x16x4_t rgba)  INLINE;
uint8x16_t pixelLumNeon16(uint8x16x4_t rgba) {
    //int R = rgbx & 0xff;
    //int G = (rgbx >> 8) & 0xff;
    //int B = (rgbx >> 16) & 0xff;

    //int Y = ( ((int)(0.299*256))*R + ((int)(0.587*256))*G + ((int)(0.114*256))*B ) >> 8;
    //int Y = ( ((int)(0.299*256))*R + ((int)(0.587*256))*G + ((int)(0.114*256))*B ) >> 8;
    uint8x16_t r = rgba.val[0];
    uint8x16_t Yr = vshrq_n_u8(r, 2);   // /4 = *0.250

    uint8x16_t g = rgba.val[1];
    uint8x16_t Yg = vshrq_n_u8(g, 1);   // /2 = *0.500

    uint8x16_t b = rgba.val[2];
    uint8x16_t Yb = vshrq_n_u8(b, 3);   // /8 = *0.125
                                        //       0.875 = Total: need a 20% boost (see DELTA_LUM_THRESHOLD)
    uint8x16_t Y = vqaddq_u8(Yr, Yg);
    Y = vqaddq_u8(Y, Yb);

    //printf("pixelLumNeon r:%d g:%d b:%d a:%d y:%d\n", vgetq_lane_u8(r, 0), vgetq_lane_u8(g, 0), vgetq_lane_u8(b, 0), vgetq_lane_u8(rgba.val[3], 0), vgetq_lane_u8(Y, 0));

    return Y;
}

uint8x16x4_t pixelMeanNeon16(uint8x16x4_t pixel1, uint8x16x4_t pixel2) INLINE;
uint8x16x4_t pixelMeanNeon16(uint8x16x4_t pixel1, uint8x16x4_t pixel2) {
    uint8x16x4_t meanPixel;

    //meanPixel[0] = (pixel1[0] + pixel2[0]) >> 1;    // R
    meanPixel.val[0] = vrhaddq_u8(pixel1.val[0], pixel2.val[0]);
    //meanPixel[1] = (pixel1[1] + pixel2[1]) >> 1;    // G
    meanPixel.val[1] = vrhaddq_u8(pixel1.val[1], pixel2.val[1]);
    //meanPixel[2] = (pixel1[2] + pixel2[2]) >> 1;    // B
    meanPixel.val[2] = vrhaddq_u8(pixel1.val[2], pixel2.val[2]);
    //meanPixel[3] = (pixel1[3] + pixel2[3]) >> 1;    // A
    meanPixel.val[3] = vrhaddq_u8(pixel1.val[3], pixel2.val[3]);

    /*printf("pixelMeanNeon r:%d %d %d  g:%d %d %d  b:%d %d %d\n",
        vgetq_lane_u8(pixel1.val[0], 0), vgetq_lane_u8(pixel2.val[0], 0), vgetq_lane_u8(meanPixel.val[0], 0),
        vgetq_lane_u8(pixel1.val[1], 0), vgetq_lane_u8(pixel2.val[1], 0), vgetq_lane_u8(meanPixel.val[1], 0),
        vgetq_lane_u8(pixel1.val[2], 0), vgetq_lane_u8(pixel2.val[2], 0), vgetq_lane_u8(meanPixel.val[2], 0));*/

    return meanPixel;
}

uint8x16_t pixelMovementNeon16(uint8x16x4_t pixel1, uint8x16x4_t pixel2) INLINE;
uint8x16_t pixelMovementNeon16(uint8x16x4_t pixel1, uint8x16x4_t pixel2) {
    //int dR = abs((int)((rawPixel1 & 0xff) - (rawPixel2 & 0xff)));
    //int dG = abs((int)(((rawPixel1 >> 8) & 0xff) - ((rawPixel2 >> 8) & 0xff)));
    //int dB = abs((int)(((rawPixel1 >> 16) & 0xff) - ((rawPixel2 >> 16) & 0xff)));

    //int dY = ( ((int)(0.299*256))*dR + ((int)(0.587*256))*dG + ((int)(0.114*256))*dB ) >> 8;

    uint8x16_t lumPixel1 = pixelLumNeon16(pixel1);
    uint8x16_t lumPixel2 = pixelLumNeon16(pixel2);
    int8x16_t deltaLum = vabdq_u8(lumPixel1, lumPixel2);        // ABS(SUB(,))

    uint8x16_t threshold = vdupq_n_u8(DELTA_LUM_THRESHOLD);
    uint8x16_t movement = vcgeq_u8(deltaLum, threshold);

    /*if (vgetq_lane_u8(movement, 0)) {
        printf("pixelMovementNeon r:%d %d  g:%d %d  b:%d %d  lum:%d %d %d  mvt:%d\n",
            vgetq_lane_u8(pixel1.val[0], 0), vgetq_lane_u8(pixel2.val[0], 0),
            vgetq_lane_u8(pixel1.val[1], 0), vgetq_lane_u8(pixel2.val[1], 0),
            vgetq_lane_u8(pixel1.val[2], 0), vgetq_lane_u8(pixel2.val[2], 0),
            vgetq_lane_u8(lumPixel1, 0), vgetq_lane_u8(lumPixel2, 0), vgetq_lane_u8(deltaLum, 0),
            vgetq_lane_u8(movement, 0));
    }*/
    return movement;
}

uint8x16x4_t pixelSwitchNeon16(uint8x16_t condition, uint8x16x4_t pixel1, uint8x16x4_t pixel2) INLINE;
uint8x16x4_t pixelSwitchNeon16(uint8x16_t condition, uint8x16x4_t pixel1, uint8x16x4_t pixel2)
{
    uint8x16x4_t outPixel;

    outPixel.val[0] = vbslq_u8(condition, pixel1.val[0], pixel2.val[0]);  // (condition ? pixel1 : pixel2)
    outPixel.val[1] = vbslq_u8(condition, pixel1.val[1], pixel2.val[1]);
    outPixel.val[2] = vbslq_u8(condition, pixel1.val[2], pixel2.val[2]);
    outPixel.val[3] = vbslq_u8(condition, pixel1.val[3], pixel2.val[3]);

    return (outPixel);
}

uint8x16x4_t pixelBoundNeon16(int8x16_t lum1, int8x16_t lum2, int8x16_t lum, uint8x16x4_t pixel1, uint8x16x4_t pixel2, uint8x16x4_t pixel) INLINE;
uint8x16x4_t pixelBoundNeon16(int8x16_t lum1, int8x16_t lum2, int8x16_t lum, uint8x16x4_t pixel1, uint8x16x4_t pixel2, uint8x16x4_t pixel)
{
    uint8x16x4_t boundedPixel;
    uint8x16_t maxLum, minLum;
    uint8x16x4_t minPixel, maxPixel;

    //if (lum1 > lum2) {
    uint8x16_t lum1GTlum2 = vcgtq_u8(lum1, lum2);
    //    maxLum = lum1;
    //    maxPixel = pixel1;
    //    minLum = lum2;
    //    minPixel = pixel2;
    //}
    //else {
    //    maxLum = lum2;
    //    maxPixel = pixel2;
    //    minLum = lum1;
    //    minPixel = pixel1;
    //}
    maxLum = vbslq_u8(lum1GTlum2, lum1, lum2);                 // (lum1GTlum2 ? lum1 : lum2)
    maxPixel = pixelSwitchNeon16(lum1GTlum2, pixel1, pixel2);

    minLum = vbslq_u8(lum1GTlum2, lum2, lum1);
    minPixel = pixelSwitchNeon16(lum1GTlum2, pixel2, pixel1);

    //if (lum > maxLum) {
    uint8x16_t lumGTmaxLum = vcgtq_u8(lum, maxLum);
    //    boundedPixel = maxPixel;
    //    printf("pixelBoundNeon max lum1:%d lum2:%d minLum:%d maxLum:%d lum:%d pixelLumNeon:%d!\n", lum1, lum2, minLum, maxLum, lum, pixelLumNeon16(boundedPixel));
    //}
    //else if (lum < minLum) {
    uint8x16_t lumLTminLum = vcltq_u8(lum, minLum);
    //    boundedPixel = minPixel;
    //    printf("pixelBoundNeon max lum1:%d lum2:%d minLum:%d maxLum:%d lum:%d pixelLumNeon:%d!\n", lum1, lum2, minLum, maxLum, lum, pixelLumNeon16(boundedPixel));
    //}
    //else {
    //    boundedPixel = pixel;
    //}
    boundedPixel = pixelSwitchNeon16(lumLTminLum, minPixel, pixel);
    boundedPixel = pixelSwitchNeon16(lumGTmaxLum, maxPixel, boundedPixel);

    /*printf("pixelBoundNeon16 p1:%d %d %d  p2:%d %d %d  p:%d %d %d  lum:%d %d %d  lGT:%d  lLT:%d  o:%d %d %d\n",
        vgetq_lane_u8(pixel1.val[0], 0), vgetq_lane_u8(pixel1.val[1], 0), vgetq_lane_u8(pixel1.val[2], 0),
        vgetq_lane_u8(pixel2.val[0], 0), vgetq_lane_u8(pixel2.val[1], 0), vgetq_lane_u8(pixel2.val[2], 0),
        vgetq_lane_u8(pixel.val[0], 0), vgetq_lane_u8(pixel.val[1], 0), vgetq_lane_u8(pixel.val[2], 0),
        vgetq_lane_u8(lum1, 0), vgetq_lane_u8(lum2, 0), vgetq_lane_u8(lum, 0),
        vgetq_lane_u8(lumGTmaxLum, 0), vgetq_lane_u8(lumLTminLum, 0),
        vgetq_lane_u8(boundedPixel.val[0], 0), vgetq_lane_u8(boundedPixel.val[1], 0), vgetq_lane_u8(boundedPixel.val[2], 0));*/

    return boundedPixel;
}

int deinterlaceNeon16(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField)
{
    int outHeight = inHeight * 2 - 1;

    //printf("height: %d\t width: %d\t inStride:%d outStride:%d\n", inHeight, inWidth, inStride, outStride);

    if (inStride % 16) {
        fprintf(stderr, "inStride=%d not supported (must be %%16)!\n", inStride);
        return -1;
    }

    // Current buf contains Even fields
    if (0 == currentBufField)
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: let's decide if we rely on odd pixels (weave) or even interpolated ones (Bob)
            for (int px = 0; px < inWidth; px += 16)
            {
                //uint32_t nextPixel = nextBuf[inLine * inStride + px];
                uint8x16x4_t nextPixel = vld4q_u8((uint8_t*)&nextBuf[inLine * inStride + px]);

                //uint32_t previousPixel = previousBuf[inLine * inStride + px];
                uint8x16x4_t previousPixel = vld4q_u8((uint8_t*)&previousBuf[inLine * inStride + px]);

                //uint32_t upPixel = currentBuf[inLine * inStride + px];
                uint8x16x4_t upPixel = vld4q_u8((uint8_t*)&currentBuf[inLine * inStride + px]);

                //uint32_t downPixel = (inLine < inHeight - 1) ?  currentBuf[(inLine + 1) * inStride + px] :
                //                                                currentBuf[inLine * inStride + px];
                int inLineT = (inLine < inHeight - 1) ? (inLine + 1) : inLine;
                uint8x16x4_t downPixel = vld4q_u8((uint8_t*)&currentBuf[inLineT * inStride + px]);

                //uint32_t upPixel2 = previous2Buf[inLine * inStride + px];
                uint8x16x4_t upPixel2 = vld4q_u8((uint8_t*)&previous2Buf[inLine * inStride + px]);

                //uint32_t downPixel2 = (inLine < inHeight - 1) ? previous2Buf[(inLine + 1) * inStride + px] :
                //                                                previous2Buf[inLine * inStride + px];
                uint8x16x4_t downPixel2 = vld4q_u8((uint8_t*)&previous2Buf[inLineT * inStride + px]);

                // Even movement?
                //bool nextMove = pixelMovementNeon16(nextPixel, previousPixel);
                uint8x16_t nextMove = pixelMovementNeon16(nextPixel, previousPixel);

                // Odd movement?
                //bool upMove = pixelMovementNeon16(upPixel, upPixel2);
                uint8x16_t upMove = pixelMovementNeon16(upPixel, upPixel2);

                //bool downMove = pixelMovementNeon16(downPixel, downPixel2);
                uint8x16_t downMove = pixelMovementNeon16(downPixel, downPixel2);

                uint8x16_t upDownMove = vandq_u8(upMove, downMove);
                //if (upMove && downMove) {
                    // Moving for real: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    //uint32_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);
#ifndef COLORED
                    //outBuf[outLine * outStride + px] = meanSpacialPixel;
                    uint8x16x4_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);
#else
                    //outBuf[outLine * outStride + px] = GREEN_PIXEL;
                    uint8x16x4_t meanSpacialPixel = pixelColorNeon16(1);
#endif
                //} else if (upMove || downMove || nextMove) {
                    uint8x16_t upDownNextMove = vorrq_u8(upMove, downMove);
                    upDownNextMove = vorrq_u8(upDownNextMove, nextMove);
                    // Something is moving around: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    // But bound lum to next and previous limits to avoid spark effect due to CDD noise.
                    //int upLum = pixelLumNeon16(upPixel);
                    uint8x16_t upLum = pixelLumNeon16(upPixel);

                    //int downLum = pixelLumNeon16(downPixel);
                    uint8x16_t downLum = pixelLumNeon16(downPixel);

                    //int meanSpacialLum = (upLum + downLum) >> 1;
                    uint8x16_t meanSpacialLum = vrhaddq_u8(upLum, downLum);

                    //int nextLum = pixelLumNeon16(nextPixel);
                    uint8x16_t nextLum = pixelLumNeon16(nextPixel);
                    
                    //int previousLum = pixelLumNeon16(previousPixel);
                    uint8x16_t previousLum = pixelLumNeon16(previousPixel);
                    
                    //uint32_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);
                    
                    //uint32_t boundedSpacialPixel = pixelBoundNeon16(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#ifndef COLORED
                    //outBuf[outLine * outStride + px] = boundedSpacialPixel;
                    uint8x16x4_t boundedSpacialPixel = pixelBoundNeon16(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#else
                    //outBuf[outLine * outStride + px] = RED_PIXEL;
                    uint8x16x4_t boundedSpacialPixel = pixelColorNeon16(0);
#endif
                //} else {
                    // Everything seams calm, let's rely on temporal pixel approximation
                    //uint32_t meanTemporalPixel = pixelMeanNeon16(nextPixel, previousPixel);
                    uint8x16x4_t meanTemporalPixel = pixelMeanNeon16(nextPixel, previousPixel);
                    //outBuf[outLine * outStride + px] = meanTemporalPixel;
                //}
                /*if (vgetq_lane_u8(upDownNextMove, 0)) {
                    printf("pixelBoundNeon16 p1:%d %d %d  p2:%d %d %d  p:%d %d %d  lum:%d %d %d  o:%d %d %d\n",
                        vgetq_lane_u8(nextPixel.val[0], 0), vgetq_lane_u8(nextPixel.val[1], 0), vgetq_lane_u8(nextPixel.val[2], 0),
                        vgetq_lane_u8(previousPixel.val[0], 0), vgetq_lane_u8(previousPixel.val[1], 0), vgetq_lane_u8(previousPixel.val[2], 0),
                        vgetq_lane_u8(meanSpacialPixel.val[0], 0), vgetq_lane_u8(meanSpacialPixel.val[1], 0), vgetq_lane_u8(meanSpacialPixel.val[2], 0),
                        vgetq_lane_u8(nextLum, 0), vgetq_lane_u8(previousLum, 0), vgetq_lane_u8(meanSpacialLum, 0),
                        vgetq_lane_u8(boundedSpacialPixel.val[0], 0), vgetq_lane_u8(boundedSpacialPixel.val[1], 0), vgetq_lane_u8(boundedSpacialPixel.val[2], 0));
                }*/
                uint8x16x4_t outPixel = pixelSwitchNeon16(upDownNextMove, boundedSpacialPixel, meanTemporalPixel);
                outPixel = pixelSwitchNeon16(upDownMove, meanSpacialPixel, outPixel);
                vst4q_u8((uint8_t*)&outBuf[outLine * outStride + px], outPixel);
            }
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    // Current buf contains Odd fields
    else
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: let's decide if we rely on even pixels (weave) or odd interpolated ones (Bob)
            for (int px = 0; px < inWidth; px += 16)
            {
                //uint32_t nextPixel = nextBuf[inLine * inStride + px];
                uint8x16x4_t nextPixel = vld4q_u8((uint8_t*)&nextBuf[inLine * inStride + px]);

                //uint32_t previousPixel = previousBuf[inLine * inStride + px];
                uint8x16x4_t previousPixel = vld4q_u8((uint8_t*)&previousBuf[inLine * inStride + px]);

                //uint32_t upPixel = (inLine > 0) ?   currentBuf[(inLine - 1) * inStride + px] : 
                //                                    currentBuf[inLine * inStride + px];
                int inLineU = (inLine > 0) ? (inLine - 1) : inLine;
                uint8x16x4_t upPixel = vld4q_u8((uint8_t*)&currentBuf[inLineU * inStride + px]);

                //uint32_t downPixel = (inLine < inHeight - 1) ?  currentBuf[(inLine) * inStride + px] :
                //                                                currentBuf[(inLine - 1) * inStride + px] ;
                int inLineD = (inLine < inHeight - 1) ? inLine : (inLine - 1);
                uint8x16x4_t downPixel = vld4q_u8((uint8_t*)&currentBuf[inLineD * inStride + px]);

                //uint32_t upPixel2 = (inLine > 0) ?  previous2Buf[(inLine - 1) * inStride + px] : 
                //                                    previous2Buf[inLine * inStride + px];
                uint8x16x4_t upPixel2 = vld4q_u8((uint8_t*)&previous2Buf[inLineU * inStride + px]);

                //uint32_t downPixel2 = (inLine < inHeight - 1) ?    previous2Buf[(inLine) * inStride + px] :
                //                                                   previous2Buf[(inLine - 1) * inStride + px] ;
                uint8x16x4_t downPixel2 = vld4q_u8((uint8_t*)&previous2Buf[inLineD * inStride + px]);

                // Odd movement?
                //bool nextMove = pixelMovementNeon16(nextPixel, previousPixel);
                uint8x16_t nextMove = pixelMovementNeon16(nextPixel, previousPixel);

                // Even movement?
                //bool upMove = pixelMovementNeon16(upPixel, upPixel2);
                uint8x16_t upMove = pixelMovementNeon16(upPixel, upPixel2);

                //bool downMove = pixelMovementNeon16(downPixel, downPixel2);
                uint8x16_t downMove = pixelMovementNeon16(downPixel, downPixel2);

                uint8x16_t upDownMove = vandq_u8(upMove, downMove);
                //if (upMove && downMove) {
                    // Moving for real: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    //uint32_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);
#ifndef COLORED
                    //outBuf[outLine * outStride + px] = meanSpacialPixel;
                    uint8x16x4_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);
#else
                    //outBuf[outLine * outStride + px] = GREEN_PIXEL;
                    uint8x16x4_t meanSpacialPixel = pixelColorNeon16(1);
#endif
                //} else if (upMove || downMove || nextMove) {
                    uint8x16_t upDownNextMove = vorrq_u8(upMove, downMove);
                    upDownNextMove = vorrq_u8(upDownNextMove, nextMove);
                    // Something is moving around: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    // But bound lum to next and previous limits to avoid spark effect due to CDD noise.
                    //int upLum = pixelLumNeon16(upPixel);
                    uint8x16_t upLum = pixelLumNeon16(upPixel);

                    //int downLum = pixelLumNeon16(downPixel);
                    uint8x16_t downLum = pixelLumNeon16(downPixel);

                    //int meanSpacialLum = (upLum + downLum) >> 1;
                    uint8x16_t meanSpacialLum = vrhaddq_u8(upLum, downLum);

                    //int nextLum = pixelLumNeon16(nextPixel);
                    uint8x16_t nextLum = pixelLumNeon16(nextPixel);

                    //int previousLum = pixelLumNeon16(previousPixel);
                    uint8x16_t previousLum = pixelLumNeon16(previousPixel);

                    //uint32_t meanSpacialPixel = pixelMeanNeon16(upPixel, downPixel);

                    //uint32_t boundedSpacialPixel = pixelBoundNeon16(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#ifndef COLORED
                    //outBuf[outLine * outStride + px] = boundedSpacialPixel;
                    uint8x16x4_t boundedSpacialPixel = pixelBoundNeon16(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#else
                    //outBuf[outLine * outStride + px] = RED_PIXEL;
                    uint8x16x4_t boundedSpacialPixel = pixelColorNeon16(0);
#endif
                //} else {
                    // Everything seams calm, let's rely on temporal pixel approximation
                    //uint32_t meanTemporalPixel = pixelMeanNeon16(nextPixel, previousPixel);
                    uint8x16x4_t meanTemporalPixel = pixelMeanNeon16(nextPixel, previousPixel);
                    //outBuf[outLine * outStride + px] = meanTemporalPixel;
                //}
                uint8x16x4_t outPixel = pixelSwitchNeon16(upDownNextMove, boundedSpacialPixel, meanTemporalPixel);
                outPixel = pixelSwitchNeon16(upDownMove, meanSpacialPixel, outPixel);
                vst4q_u8((uint8_t*)&outBuf[outLine * outStride + px], outPixel);
            }
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    return 0;
}
#endif // #if defined (__ARM_NEON)
