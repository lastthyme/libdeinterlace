/*
 * Copyright (c) 2020 Renault S.A.S.
 * This Source Code Form is subject to the terms of the Mozilla Public * License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * you can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>
#include "deinterlace.h"

void usage(char *name) {
	fprintf(stderr, "Usage: %s WidthxHeight\n", name);
	fprintf(stderr, "	stdin: interlaced rgba raw frames\n");
	fprintf(stderr, "	stdout: deinterlaced rgba raw frames\n");
}

int main(int argc, char** argv)
{
	int width=0, height=0;

    if (argc != 2) {
		usage(argv[0]);
		return(-1);
	}

	int ret=sscanf(argv[1], "%dx%d", &width, &height);
	if (ret !=2) {
		usage(argv[0]);
		return(-1);
	}
	fprintf(stderr, "Resolution: %dx%d\n", width, height);


	int size=width * height;
	const int nbInputBuffers=3;
	uint32_t *inputBuffer[nbInputBuffers];
	uint32_t *outputBuffer=(uint32_t *)malloc(size * sizeof(uint32_t));
	for (int i=0 ; i<nbInputBuffers ; i++) {
		inputBuffer[i]=(uint32_t *)malloc(size * sizeof(uint32_t));
	}

	int i=0;
	do {
		if (fread(inputBuffer[i%nbInputBuffers], size * sizeof(uint32_t), 1, stdin) > 0) {
			fprintf(stderr, "Frame: %d\n", i);

			if (i >= (nbInputBuffers-1)) {
				// Odd+even
				fprintf(stderr, "Deinterlace frame %d odd field + frame %d even field\n", i-1, i);
	    		deinterlace(inputBuffer[(i-2)%nbInputBuffers]+width,
							inputBuffer[(i-1)%nbInputBuffers],
							inputBuffer[(i-1)%nbInputBuffers]+width,
							inputBuffer[(i-0)%nbInputBuffers],
							height/2,
							width,
							width*2,
							width,
							outputBuffer,
							1);
				fwrite(outputBuffer, sizeof(uint32_t), size, stdout);

				// Even+odd
				fprintf(stderr, "Deinterlace frame %d even field + frame %d odd field\n", i, i);
	    		deinterlace(inputBuffer[(i-1)%nbInputBuffers],
							inputBuffer[(i-1)%nbInputBuffers]+width,
							inputBuffer[(i-0)%nbInputBuffers],
							inputBuffer[(i-0)%nbInputBuffers]+width,
							height/2,
							width,
							width*2,
							width,
							outputBuffer,
							0);
				fwrite(outputBuffer, sizeof(uint32_t), size, stdout);
			}

			i+=1;
		} else {
			break;
		}
	} while (1);

	for (int i=0 ; i<nbInputBuffers ; i++) {
		free(inputBuffer[i]);
	}
	free(outputBuffer);

	return(0);
}
