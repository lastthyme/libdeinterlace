/*
 * Copyright (c) 2020 Renault S.A.S.
 * This Source Code Form is subject to the terms of the Mozilla Public * License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * you can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <stdint.h>

int pixelLum(uint32_t rgbx);

int weave(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField) __attribute__( ( noinline ) );

int deinterlace(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField) __attribute__( ( noinline ) );

int deinterlaceNeon16(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField) __attribute__( ( noinline ) );
